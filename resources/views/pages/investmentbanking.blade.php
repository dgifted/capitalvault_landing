@extends('layouts.master')

@section('title', 'Investment banking')
@section('styles')
@stop

@section('header')
<header class="header-image ken-burn-center light" data-parallax="true" data-natural-height="500"
        data-natural-width="1920" data-bleed="0" data-image-src="{{asset('media/hd-wide-2.jpg')}}" data-offset="0">
        <div class="container">
            <h1>Investment Banking</h1>
            <h2>Harvesting deep expertise to deliver high-quality
                mergers and acquisitions (M&A) and private equity (PE) advisory services </h2>
        </div>
    </header>
@stop

@section('content')
<section class="section-base">
            <div class="container">
                <div class="row row-fit-lg">
                    <div class="col-lg-4">
                        <p>
                            Investment Banking is part of {{ parse_url(config('app.url'))['host'] }} business. Deep domain expertise, an attitude
                            of collaboration to craft win-win situations, a global outlook and outcome driven approach
                            are the hallmark of our services.<br>We provide seamless solutions to a wide range of
                            clients by leveraging our teams across various industries, multiple product groups and
                            geographies. Our rich experience and strong institutional investor relationships help
                            clients meet their strategic, financing and growth objectives.<br>From growth stage funding
                            to complex, large sized transactions later in the cycle, Avendus Capital serves clients
                            across the spectrum,ranging from mid-market companies to large global organizations with
                            multinational operations.
                        </p>
                    </div>
                    <div class="col-lg-4">
                        <p>
                            The spirit of entrepreneurship is at the heart of every relationship that we nurture.<br>Our
                            client-first mindset helps us craft unique and bespoke solutions for entrepreneurs,
                            investors and board of directors. We have a strong track record of cross-border transactions
                            and have helped multiple clients benefit from opportunities across geographies. The robust
                            pool of regulatory knowledge within {{ parse_url(config('app.url'))['host'] }} helps us structure and execute optimal
                            solutions for our clients in an efficient manner.
                        </p>
                    </div>
                    <div class="col-lg-4">
                        <ul class="slider light" data-options="arrows:false,nav:true">
                            <li>
                                <a class="img-box lightbox" href="{{asset('media/image-1.jpg')}}">
                                    <img src="{{asset('media/image-1.jpg')}}" alt="">
                                </a>
                            </li>
                            <li>
                                <a class="img-box lightbox" href="{{asset('media/image-1.jpg')}}">
                                    <img src="{{asset('media/image-3.jpg')}}" alt="">
                                </a>
                            </li>
                            <li>
                                <a class="img-box lightbox" href="{{asset('media/image-1.jpg')}}">
                                    <img src="{{asset('media/image-4.jpg')}}" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
            </div>
            </td>
            </tr>
            </tbody>
            </table>
            </div>
        </section>
@stop

@section('scripts')
@endsection