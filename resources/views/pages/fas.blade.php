@extends('layouts.master')

@section('title', 'Freuently asked questions')
@section('styles')
@stop

@section('header')
<header class="header-image ken-burn-center light" data-parallax="true" data-natural-height="500"
        data-natural-width="1920" data-bleed="0" data-image-src="{{asset('media/hd-wide-7.jpg')}}" data-offset="0">
        <div class="container">
            <h1>Have questions ?</h1>
            <h2>Questions and answers</h2>
        </div>
    </header>
@stop
@section('content')
<section class="section-base">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <h3>General questions</h3>
                        <hr class="space-sm">
                        <ul class="accordion-list">
                            <li>
                                <a href="#">Is {{ parse_url(config('app.url'))['host'] }} Limited incorporated?</a>
                                <div class="content">
                                    <p>
                                        {{ parse_url(config('app.url'))['host'] }} Limited is a legal financial investment company incorporated
                                        in Switzerland. Registered Office address: Tösstalstrasse 60, Watt, 9306
                                        Switzerland.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <a href="#">Who is qualified to open an account with {{ parse_url(config('app.url'))['host'] }} ?</a>
                                <div class="content">
                                    <p>
                                        Any individual (except for persons under the age of 18; as well as citizens of
                                        any countries where the Company does not provide their specified services).
                                    </p>
                                </div>
                            </li>
                            <li>
                                <a href="#"> How do I start investment with {{ parse_url(config('app.url'))['host'] }}?</a>
                                <div class="content">
                                    <p>
                                        All you need to pass a simple registration. After registration, login into your
                                        {{ parse_url(config('app.url'))['host'] }} account then go to the investment section and open your first
                                        deposit.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <a href="#"> How may I become a client of {{ parse_url(config('app.url'))['host'] }}?</a>
                                <div class="content">
                                    <p>
                                        You may become a client of {{ parse_url(config('app.url'))['host'] }} Limited and it is totally free of
                                        charge. All you need is to sign up and fill all required fields..
                                    </p>
                                </div>
                            </li>
                            <li>
                                <a href="#"> Is it free of charge to open an account? </a>
                                <div class="content">
                                    <p>
                                        Yes, it is totally free of charge.
                                    </p>
                                </div>
                            </li>
                        </ul>
                        <hr class="space">
                        <h3>Account Questions</h3>
                        <hr class="space-sm">
                        <ul class="accordion-list">
                            <li>
                                <a href="#">How long does it take to make my client account active?</a>
                                <div class="content">
                                    <p>
                                        Your account will be active immediately after registration.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <a href="#"> How may I access my account?</a>
                                <div class="content">
                                    <p>
                                        You may log into your account by clicking the link Login and enter your username
                                        and password.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <a href="#"> How can I control my account?</a>
                                <div class="content">
                                    <p>
                                        In order to control your account you need to use navigation menu in the left
                                        side of our website.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <a href="#">May I change my account details?</a>
                                <div class="content">
                                    <p>
                                        You may change your account details on Profile page.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <a href="#">How secure user accounts and personal data?</a>
                                <div class="content">
                                    <p>
                                        All stored data on our servers remains protected via encryption technology all
                                        the time. All account related transactions done by our clients are mediated
                                        exclusively via secured internet connections.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <a href="#">How many accounts can I open on the site?
                                </a>
                                <div class="content">
                                    <p>
                                        Any client can have only one account. In the event of multiple registrations
                                        from your device we have rights to suspend all of your accounts.
                                    </p>
                                </div>
                            </li>
                        </ul>
                        <hr class="space">
                        <h3>Deposits Questions</h3>
                        <hr class="space-sm">
                        <ul class="accordion-list">
                            <li>
                                <a href="#">How may I make a deposit?</a>
                                <div class="content">
                                    <p>
                                        You may make your deposit by choosing any investment package of your choice then
                                        type your desired amount in the 'Amount' field, choose payment system and click
                                        'Proceed'. Follow further instructions of your payment system.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <a href="#">When the deposit should be activated?</a>
                                <div class="content">
                                    <p>
                                        Your deposit should be activated immediately if you use Perfect Money. If you
                                        use cryptocurrencies it could be some time which is required for getting
                                        confirmations by the cryptocurrency network. For deposits in cryptocurrencies we
                                        need at least 1 confirmation. If your deposit hasn't appear in your account for
                                        a long time, please contact us.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <a href="#">Where can I read about the investment plans?</a>
                                <div class="content">
                                    <p>
                                        You can check all actual investment plans in your member area on Make Deposit
                                        page.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <a href="#">How can I withdraw my profit?</a>
                                <div class="content">
                                    <p>
                                        In order to withdraw your profit you need to naigate to "cashout" page in your
                                        Cabinet. Please input payout amount and choose payment system which you have
                                        used for making your deposits.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <a href="#">How soon after withdrawal request will the money appear on my payment
                                    account?</a>
                                <div class="content">
                                    <p>
                                        Your request will be processed instantly. We do everything possible to reduce
                                        awaiting time of our clients.
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>

        </section>
@stop

@section('scripts')
@endsection