@extends('layouts.master')

@section('title', 'NFP plans')
@section('styles')
@stop

@section('header')
<header class="header-image ken-burn-center light" data-parallax="true" data-natural-height="500"
        data-natural-width="1920" data-bleed="0" data-image-src="{{asset('media/hd-wide-5.jpg')}}" data-offset="0">
        <div class="container">
            <h1>NON-FARM PAYROLL</h1>
            <h2>Trading the Non-Farm Payroll Report</h2>
        </div>
    </header>
@stop

@section('content')
<div class="title">
            <center>
                <h2>What is the NFP?</h2>
            </center>
        </div>
        <center>
            <p>The non-farm payroll (NFP) figure is a key economic indicator for the United States economy. It
                represents the number of jobs added, excluding farm employees, government employees, private household
                employees and employees of nonprofit organizations. </p>
        </center><br><br>

        <center>
            <h4>NFP releases generally cause large movements in the forex market. The NFP data is normally released on
                the first Friday of every month at 8:30 AM ET.</h4>
        </center>
        <p>NFP data is important because it is released monthly, making it a very good indicator of the current state of
            the economy. The data is released by the Bureau of Labor Statistics and the next release can be found on an
            economic calendar.<br>Employment is a very important indicator to the Federal Reserve Bank. When
            unemployment is high, policy makers tend to have an expansionary monetary policy (stimulatory, with low
            interest rates). The goal of an expansionary monetary policy is to increase economic output and increase
            employment.
            Due to the volatile nature of the NFP release, UpWealth Finances uses pull-back strategy rather than a
            breakout strategy. Using a pullback strategy, traders should wait for the currency pair to retrace before
            entering a trade.</p>
        <div class="title">
            <center>
                <h2>NFP INVESTMENT</h2>
            </center>
        </div>
        <section class="section-base section-color">
            <div class="container">
                <div class="row" data-anima="fade-bottom" data-timeline="asc" data-time="2000">
                    <div class="col-lg-4 anima">
                        <div class="">
                            <div class="">
                                <h2></h2>
                                <div class=""><span></span></div>
                                <p></p>
                            </div>
                            <ul>

                            </ul>
                            <div class="">
                                <a class="" href="#"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 anima">
                        <div class="cnt-box cnt-pricing-table pricing-table-big">
                            <div class="top-area">
                                <h2>NFP PLAN</h2>
                                <div class="price"><span>36</span>%</div>
                                <p>FIXED INTEREST MONTHLY FOR 1 YEAR</p>
                            </div>
                            <ul>
                                <li>Minimum deposit: $5,000.00</li>
                                <li>Maximum deposit: $4,000,000.00</li>
                                <li>Return on Investment: 432%</li>
                                <li>Deposit Principal: Returned at End of Contract</li>
                                <li>Payout on each closing session</li>
                                <li>Pull-back traded stragegy</li>
                            </ul>
                            <div class="bottom-area">
                                <a class="btn btn-border btn-xs" href="#">Purchase now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 anima">
                        <div class="">
                            <div class="">
                                <h2></h2>
                                <div class=""><span></span></div>
                                <p></p>
                            </div>
                            <ul>

                            </ul>
                            <div class="">
                                <a class="" href="#"></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <div class="col-lg-6 col-md-6">
            <img src="{{asset('media/box-5.png')}}" alt="" />
        </div>
        </div>
        <section class="section-base">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <ul class="text-list text-list-side">
                            <li>
                                <h3>NFP data is released on the first Friday of every month.</h3>
                                <div></div>
                            </li>
                            <li>
                                <h3>The NFP data release is accompanied with increased volatility and widening spreads.
                                </h3>
                                <div></div>
                            </li>
                            <li>
                                <h3>Currency pairs not related to the US Dollar could also see increased volatility and
                                    widening spreads.</h3>
                                <div></div>
                            </li>
                        </ul>
                    </div>
        </section>
@stop

@section('scripts')
@endsection