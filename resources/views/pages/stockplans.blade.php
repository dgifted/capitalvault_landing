@extends('layouts.master')

@section('title', 'Stock plans')
@section('styles')
@stop

@section('header')
<header class="header-image ken-burn-center light" data-parallax="true" data-natural-height="500"
        data-natural-width="1920" data-bleed="0" data-image-src="media/hd-wide-5.jpg" data-offset="0">
        <div class="container">
            <h1>EARN BIG ON THE STOCK MARKET</h1>
            <h2>Every day the news brings changes to the price of giant corporation’s shares.
                Use it to make profit!</h2>
        </div>
    </header> 
@stop

@section('content')
<div class="title">
            <center>
                <h2>What is stock trading?</h2>
            </center>
</div>
        <center>
            <p>Stock traders buy and sell stocks to capitalize on daily price fluctuations. These short-term traders are
                betting that they can make a few bucks in the next minute, hour, day or month, rather than buying stock
                in a blue-chip company to hold for years or even decades. </p>
        </center><br><br>

        <center>
            <h2>There are two main types of stock trading:</h2>
        </center>
        <div>
            <h4>Active trading:</h4> This is an investor who places 10 or more trades per month does. Typically, they
            use a
            strategy that relies heavily on timing the market, trying to take advantage of short-term events (at the
            company
            level or based on market fluctuations) to turn a profit in the coming weeks or months.
            <h4>Day trading:</h4> is the strategy employed by investors who play hot potato with stocks — buying,
            selling
            and closing their positions of the same stock in a single trading day, caring little about the inner
            workings of
            the underlying businesses. (Position refers to the amount of a particular stock or fund you own.) The aim of
            the
            day trader is to make a few bucks in the next few minutes, hours or days based on daily price fluctuations.
        </div>
        <div class="title">
            <center>
                <h2>STOCK INVESTMENT</h2>
            </center>
        </div>
        <section class="section-base section-color">
            <div class="container">
                <div class="row" data-anima="fade-bottom" data-timeline="asc" data-time="2000">
                    <div class="col-lg-4 anima">
                        <div class="cnt-box cnt-pricing-table">
                            <div class="top-area">
                                <h2>CORE</h2>
                                <div class="price"><span>20</span>%</div>
                                <p>FIXED WEEKLY INTEREST FOR 2 MONTHS</p>
                            </div>
                            <ul>
                                <li>Minimum deposit: $1,000.00</li>
                                <li>Maximum deposit: $30,000.00</li>
                                <li>Return on Investment: 160%</li>
                                <li>No tax deductions</li>
                                <li>Personalized portfolio</li>
                                <li>Deposit Principal: Returned</li>
                            </ul>
                            <div class="bottom-area">
                                <a class="btn btn-border btn-xs" href="indexcca3.html?a=signup">Purchase now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 anima">
                        <div class="cnt-box cnt-pricing-table pricing-table-big">
                            <div class="top-area">
                                <h2>BOND</h2>
                                <div class="price"><span>25</span>%</div>
                                <p>FIXED WEEKLY INTEREST FOR 3 MONTHS</p>
                            </div>
                            <ul>
                                <li>Minimum deposit: $30,000.00</li>
                                <li>Maximum deposit: $60,000.00</li>
                                <li>Return on Investment: 300%</li>
                                <li>No tax deductions</li>
                                <li>Financial planning session</li>
                                <li>Deposit Principal: Returned</li>
                            </ul>
                            <div class="bottom-area">
                                <a class="btn btn-border btn-xs" href="indexcca3.html?a=signup">Purchase now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 anima">
                        <div class="cnt-box cnt-pricing-table">
                            <div class="top-area">
                                <h2>SHARES</h2>
                                <div class="price"><span>30</span>%</div>
                                <p>FIXED WEEKLY INTEREST FOR 5 MONTHS</p>
                            </div>
                            <ul>
                                <li>Minimum deposit: $60,000.00</li>
                                <li>Maximum deposit: $200,000.00</li>
                                <li>Return on Investment: 600%</li>
                                <li>Personal investment report</li>
                                <li>Asset allocation</li>
                            </ul>
                            <div class="bottom-area">
                                <a class="btn btn-border btn-xs" href="indexcca3.html?a=signup">Purchase now</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <section class="section-base">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <ul class="text-list text-list-side">
                            <li>
                                <h3>High volatility</h3>
                                <p>
                                    High volatility is always a great chance to boost your profit.
                                </p>
                                <div></div>
                            </li>
                            <li>
                                <h3>A way to diversify your portfolio</h3>
                                <p>
                                    The stock market differs in factors affecting its direction. It means you will earn
                                    even when others fail
                                </p>
                                <div></div>
                            </li>
                            <li>
                                <h3>Respect</h3>
                                <p>
                                    Stocks are related to Wall Street, big companies and big money. Everyone wants it!
                                </p>
                                <div></div>
                            </li>
                        </ul>
                    </div>
        </section>
@stop

@section('scripts')
@endsection