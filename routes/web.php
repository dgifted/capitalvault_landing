<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::view('/', 'pages.index')->name('home');
Route::view('about', 'pages.about')->name('about');
Route::view('purpose-value', 'pages.purposevalue')->name('values');
Route::view('regulatory-information', 'pages.regulatoryinfo')->name('reginfo');
Route::view('asset-management', 'pages.assetmgt')->name('assetmgt');
Route::view('investment-banking', 'pages.investmentbanking')->name('investbank');
Route::view('faq', 'pages.fas')->name('faq');
Route::view('contact-us', 'pages.contactus')->name('contact');
Route::view('crypto-plans', 'pages.cryptoplans')->name('cryptoplans');
Route::view('realestate-plans', 'pages.realestateplans')->name('realplans');
Route::view('stocks-plans', 'pages.stockplans')->name('stockplans');
Route::view('nfp-plans', 'pages.nfpplans')->name('nfpplans');
Route::view('privacy', 'pages.privacy')->name('privacy');

//Admin login
Route::get('/admin', function() {
    $url = config('app.app_url') . '/admin';
    return redirect($url);
});
