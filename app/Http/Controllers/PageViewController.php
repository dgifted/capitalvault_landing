<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class PageViewController extends Controller {
     public function indexPage()
     {
        return view('pages.index');
     }

     public function aboutUsPage()
     {

     }

     public function purposeValue()
     {

     }

     public function regulatoryInfo() 
     {

     }

     public function assetsMgt()
     {

     }

     public function investmentBanking()
     {

     }

     public function faq()
     {

     }

     public function contactUs()
     {

     }

     public function cryproPlans()
     {}
     
     public function realestatePlans()
     {}

     public function stockPlans()
     {}

     public function nfpPlans()
     {}

     public function login()
     {}

     public function register()

}