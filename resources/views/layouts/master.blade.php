<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') | Digital Asset Investment Strategies for Professionals and Beginners. Backed by a team
        with deep experience in finance. 🚀</title>
    <meta name="description"
        content="We provide technology-driven wealth and investment management solutions for wealth managers, investment managers, and institutional and private investors.">
   
    <meta name="designer" href="{{config('app.name')}}">
    
    <script src="{{asset('themekit/scripts/jquery.min.js')}}"></script>
    <script src="{{asset('themekit/scripts/main.js')}}"></script>
    <link rel="stylesheet" href="{{asset('themekit/css/bootstrap-grid.css')}}">
    <link rel="stylesheet" href="{{asset('themekit/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('themekit/css/glide.css')}}">
    <link rel="stylesheet" href="{{asset('themekit/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('themekit/css/content-box.css')}}">
    <link rel="stylesheet" href="{{asset('themekit/css/media-box.css')}}">
    <link rel="stylesheet" href="{{asset('themekit/css/contact-form.css')}}">
    <link rel="stylesheet" href="{{asset('skin.css')}}">
     <link rel="icon" href="{{asset('favicon.png')}}">
    <link rel="stylesheet" href="pricing.css">
    @yield('styles')
</head>
<body>
<div id="preloader"></div>
<nav class="menu-classic menu-fixed menu-transparent light align-right" data-menu-anima="fade-in">
        <div class="container">
            <div class="menu-brand">
                <a href="#">
                    <img class="logo-default scroll-hide" src="{{asset('media/_logo.png')}}" alt="logo" />
                    <img class="logo-retina scroll-hide" src="{{asset('media/_logo.png')}}" alt="logo" />
                    <img class="logo-default scroll-show" src="{{asset('media/_logo.png')}}" alt="logo" />
                    <img class="logo-retina scroll-show" src="{{asset('media/_logo.png')}}" alt="logo" />
                </a>
                <div class="notification-text-block">
                    <div class="notification-title">
                        <!-- Notification Title -->

                        <!-- / Notification Title -->
                    </div>
                    <div class="notification-text"></div>
                </div>
            </div>
        </div>
        </div>
        <i class="menu-btn"></i>
        <div class="menu-cnt">
            <ul id="main-menu">

                <li><a href="indexbc14.html?a=home">Home</a></li>

                <li class="dropdown">
                    <a href="#">Who We Are</a>
                    <ul>
                        <li><a href="{{ route('about')}}">ABOUT US</a></li>
                        <li><a href="{{route('values')}}">PURPOSE AND VALUES</a></li>
                        <li><a href="{{ route('reginfo')}}">REGULATORY INFORMATION</a></li>
                    </ul>

                <li class="dropdown">
                    <a href="#">Services</a>
                    <ul>
                        <li><a href="{{ route('assetmgt')}}">ASSET MANAGEMENT</a></li>
                        <li><a href="{{ route('investbank')}}">INVESTMENT BANKING</a></li>
                    </ul>
                <li class="dropdown">
                    <a href="#">Support</a>
                    <ul>
                        <li><a href="{{ route('faq')}}">FAQ</a></li>
                        <li><a href="{{ route('contact')}}">CONTACT US</a></li>
                    </ul>
                <li class="dropdown">
                    <a href="#">Plans</a>
                    <ul>
                        <li><a href="{{ route('cryptoplans')}}">CRYPTO ASSET PLANS</a></li>
                        <li><a href="{{ route('realplans')}}">REAL ESTATE PLANS</a></li>
                        <li><a href="{{ route('stockplans')}}">STOCK PLANS</a></li>
                        <li><a href="{{ route('nfpplans')}}">NFP PLAN</a></li>
                    </ul>


                <li><a href="{{ config('app.app_url') . '/login' }}">Login</a></li>
                <li><a href="{{ config('app.app_url') . '/register' }}">Signup</a></li>
            </ul>

        </div>
        </div>
    </nav>
    @yield('header')
    <main>
        @yield('content')
    </main>

    <i class="scroll-top-btn scroll-top show"></i>
    <footer class="light">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <a href="{{ route('home') }}">
                        <img class="" src="{{asset('media/_logo.png')}}" alt="" style="width:144px;"></a>
                    <p>We strive to create value for our clients and employees while adhering to principles of
                        excellence and trust</p>

                </div>
                <div class="col-lg-4">
                    <h3>Who We Are</h3>
                    <ul class="icon-list icon-line">

                        <li><a href="{{ route('about') }}">ABOUT US</a></li>
                        <li><a href="{{ route('values') }}">PURPOSE AND VALUES</a></li>
                    </ul>
                </div>
                <div class="col-lg-4">
                    <h3>Useful Links</h3>
                    <ul class="icon-list icon-line">
                        <li><a href="{{ route('home') }}">HOME</a></li>

                        <li><a href="{{ route('faq') }}">FAQ</a></li>
                        <li><a href="{{ route('contact') }}">CONTACT US</a></li>
                        <li><a href="{{ config('app.app_url') . '/login' }}">LOGIN</a></li>
                        <li><a href="{{ config('app.app_url') . '/register' }}">REGISTER</a></li>

                    </ul>
                </div>
                <div class="col-lg-4">
                    <ul class="text-list text-list-line">
                        <li><b>Address:</b>
                            <hr />
                            <p>Tösstalstrasse 60,<br /> Watt, 9306 Switzerland.</p>
                        </li><br>
                        <li><b>Support Email</b>
                            <hr />
                            <p><a href="#">{{ config('mail.support_email') }}</a></p>
                        </li>


                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-bar">
            <div class="container">
                <span>Copyright {{ now()->year }}</span><br>
                <span><a href="{{ route('contact') }}">Support</a> | <a href="{{ route('privacy') }}">Privacy
                        policy</a></span>
            </div>
        </div>
        <link rel="stylesheet" href="{{asset('themekit/media/icons/iconsmind/line-icons.min.css')}}">
        <script src="{{asset('themekit/scripts/parallax.min.js')}}"></script>
        <script src="{{asset('themekit/scripts/glide.min.js')}}"></script>
        <script src="{{asset('themekit/scripts/magnific-popup.min.js')}}"></script>
        <script src="{{asset('themekit/scripts/tab-accordion.js')}}"></script>
        <script src="{{asset('themekit/scripts/imagesloaded.min.js')}}"></script>
        <script src="{{asset('themekit/scripts/progress.js')}}" async></script>
        <script src="{{asset('kit.fontawesome.com/a0c361a166.js')}}" crossorigin="anonymous"></script>

    </footer>

    <link rel="stylesheet" type="text/css" href="note.css">
<script src="{{asset('note.js')}}"></script>

<script type="text/javascript">
    var country_list = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua & Barbuda", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Belarus", "Belgium", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia & Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burundi", "Cambodia", "Cameroon", "Cape Verde", "Chad", "Chile", "China", "Colombia", "Congo", "Costa Rica", "Cote D Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Gibraltar", "Greece", "Greenland", "Grenada", "Guatemala", "Guinea", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kuwait", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Mauritania", "Mauritius", "Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Norway", "Oman", "Pakistan", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre & Miquelon", "San Marino", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "South Africa", "South Korea", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad & Tobago", "Tunisia", "Turkey", "Turkmenistan", , "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "Uzbekistan", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen"];

    function choice(array) {
        return array[Math.floor(Math.random() * array.length)];
    }

    function randomNumber(min, max) {
        return Math.random() * (max - min) + min;
    }


    function payinout() {
        var nu = Math.floor(randomNumber(100, 7500));
        var con = choice(country_list);
        var mode = choice(['deposited', 'withdrew']);

        spop({
            template: "An Investor from " + con + " " + mode + " $" + nu + " ...",

            position: 'bottom-center',
            style: 'success',
            autoclose: 4000,
        });

    }

    setInterval(payinout, 7000);

</script>
<div class="gtranslate_wrapper"></div>
<script>
window.gtranslateSettings = { "default_language": "en", "wrapper_selector": ".gtranslate_wrapper" 
}</script>
<script src="{{asset('cdn.gtranslate.net/widgets/latest/float.js')}}" defer></script>


<!-- TradingView Widget BEGIN -->
<div class="tradingview-widget-container" style="width:100%; height:88px;">
    <div class="tradingview-widget-container__widget"></div>
    <div class="tradingview-widget-copyright"><a href="#" rel="noopener" target="_blank"><span
                class="blue-text"></span></a> </div>
    <!-- <script data-cfasync="false" src="cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.html"></script> -->
    <script type="text/javascript" src="{{asset('s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js')}}" async>
        {
            "symbols": [
                {
                    "proName": "FOREXCOM:SPXUSD",
                    "title": "S&P 500"
                },
                {
                    "proName": "FOREXCOM:NSXUSD",
                    "title": "Nasdaq 100"
                },
                {
                    "proName": "FX_IDC:EURUSD",
                    "title": "EUR/USD"
                },
                {
                    "proName": "BITSTAMP:BTCUSD",
                    "title": "BTC/USD"
                },
                {
                    "proName": "BITSTAMP:ETHUSD",
                    "title": "ETH/USD"
                }
            ],
                "colorTheme": "dark",
                    "isTransparent": false,
                        "displayMode": "adaptive",
                            "locale": "en"
        }
    </script>

    <script src="{{asset('code.tidio.co_443/3lpp9tgd7ximpgiv8qhpvdzo9zdpxwi4.js')}}" async>

    </script>
    @yield('scripts')
</body>

</html>