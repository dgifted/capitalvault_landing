@extends('layouts.master')

@section('title', 'Home')
@section('styles')
@stop

@section('content')
<section class="section-video section-home-slider light">
            <video autoplay loop muted poster="media/video-poster2.jpg">
                <source src="{{asset('media/video.html')}}" type="video/mp4">
            </video>
            <div class="container">
                <hr class="space" />
                <div class="row">
                    <div class="col-lg-7">
                        <ul class="slider" data-options="type:slider,perView:1,arrows:true,nav:true,autoplay:3500"
                            data-anima="fade-right" data-time="2000">
                            <li>
                                <h1>Pioneers in
                                    digital asset investing</h1>
                                <a class="img-box" href="#">
                                    <img src="{{asset('media/image-8.jpg')}}" alt="">
                                </a>
                            </li>
                            <li>
                                <h1>Wealth and Investment Management Solutions</h1>
                                <a class="img-box" href="#">
                                    <img src="{{asset('media/image-3.jpg')}}" alt="">
                                </a>
                            </li>
                            <li>
                                <h1>Investments For All Market Conditions</h1>
                                <a class="img-box" href="#">
                                    <img src="{{asset('media/image-6.jpg')}}" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-5">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="grid-list gap-18" data-columns="1" data-columns-md="3" data-columns-sm="3"
                                    data-columns-xs="1">
                                    <div class="grid-box">
                                        <div class="grid-item">
                                            <div class="" data-anima="fade-right" data-time="3000">
                                                <div>
                                                    <h3></h3>
                                                    <div class="value">
                                                        <span
                                                            style=" font-size: 25px; line-height: 35px; font-weight: 700; letter-spacing: .5px; color: #03BFCB; "></span>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="grid-item">
                                            <div class="" data-anima="fade-right" data-time="3000">
                                                <div>
                                                    <h3></h3>
                                                    <div class="value">
                                                        <span
                                                            style=" font-size: 25px; line-height: 35px; font-weight: 700; letter-spacing: .5px; color: #03BFCB; "></span>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="grid-item">
                                            <div class="" data-anima="fade-right" data-time="3000">
                                                <div>
                                                    <h3></h3>
                                                    <div class="value">
                                                        <span
                                                            style=" font-size: 25px; line-height: 35px; font-weight: 700; letter-spacing: .5px; color: #03BFCB; "></span>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8" data-anima="fade-right" data-time="4000">
                                <h2><span>Our</span> Evolution </h2>
                                <p><b>
                                        We believe that Bitcoin and blockchain networks are landmark innovations that
                                        will fundamentally reshape the global financial system, and investors should be
                                        able to participate in this transformation.</b>
                                </p>
                                <a href="index15a0.html?a=support" class="btn btn-sm">Contact us</a>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="space" />
                <hr class="space" />
            </div>
        </section>
        <section class="section-base section-color section-overflow-top">
            <div class="container">
                <div class="grid-list" data-columns="3" data-columns-md="1">
                    <div class="grid-box">
                        <div class="grid-item">
                            <div class="cnt-box cnt-box-info boxed" data-href="#">
                                <a href="#" class="img-box"><img src="{{asset('media/image-3s.jpg')}}" alt="" /></a>
                                <div class="caption">
                                    <h2>Active Strategies</h2>
                                    <p>
                                        Our managed strategies serve investors who prefer more tailored investment
                                        approaches. Access to these strategies are available to all accredited
                                        investors.
                                        Our actively traded strategies provide managed exposure to digital assets.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item">
                            <div class="cnt-box cnt-box-info boxed" data-href="#">
                                <a href="#" class="img-box"><img src="{{asset('media/image-4.jpg')}}" alt="" /></a>
                                <div class="caption">
                                    <h2>Capital Markets</h2>
                                    <p>
                                        {{ parse_url(config('app.url'))['host'] }} Capital Markets matches human capital with purpose-built
                                        infrastructure and systems to help institutional counterparties and private
                                        investors meet their unique liquidity needs through a full suite of trading,
                                        risk management, and hedging services.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="grid-item">
                            <div class="cnt-box cnt-box-info boxed" data-href="#">
                                <a href="#" class="img-box"><img src="{{asset('media/loan.jpg')}}" alt="" /></a>
                                <div class="caption">
                                    <h2>Loan</h2>
                                    <p>
                                        At Opulence Crest, we understand that financial needs vary, and sometimes you
                                        just need a little extra support. That's why we offer straightforward loans to
                                        individuals. Whether you're looking to fund a project, cover unexpected
                                        expenses, or simply need some financial flexibility, we're here to help. Our
                                        application process is easy, and our team is committed to providing the support
                                        you need. Your financial well-being is our priority, and we're ready to assist
                                        you on your journey. Apply for a loan with Opulence Crest today..
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- TradingView Widget BEGIN -->
                <div class="tradingview-widget-container">
                    <div id="tradingview_dbf59"></div>
                    <div class="tradingview-widget-copyright"><a href="{{url('https://www.tradingview.com/')}}"
                            rel="noopener nofollow" target="_blank"><span class="blue-text"></span></a></div>
                    <script type="text/javascript" src="{{asset('s3.tradingview.com/tv.js')}}"></script>
                    <script type="text/javascript">
                        new TradingView.widget(
                            {
                                "width": "345",
                                "height": "410",
                                "symbol": "NASDAQ:AAPL",
                                "interval": "D",
                                "timezone": "Etc/UTC",
                                "theme": "light",
                                "style": "1",
                                "locale": "en",
                                "enable_publishing": false,
                                "allow_symbol_change": true,
                                "container_id": "tradingview_dbf59"
                            }
                        );
                    </script>
                </div>
                <!-- TradingView Widget END -->
            </div>
            <img src="images/o1.jpg" alt="" width="100%" />
        </section>
        <section class="section-base">
            <div class="container">
                <div class="row align-items-center" data-anima="fade-bottom" data-time="1000">
                    <div class="col-lg-6 col-md-6">
                        <img src="{{asset('media/box-1.jpg')}}" alt="" />
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="title">
                            <h2>OIL & GAS</h2>
                            <p></p>
                        </div>
                        <p>
                            Oil accounts for approximately 3% of GDP and is one of the most important commodities in the
                            world � petroleum products can be found in everything from personal protective equipment,
                            plastics, chemicals and fertilisers through to aspirin, clothing, fuel for transportation
                            and even solar panels. Its A Steady Investment


                        </p>
                        <a href="indexe47e.html?a=about" class="btn btn-xs">Learn more</a>
                    </div>
                </div>
                <hr class="space-sm" />
                <hr class="space visible-md" />
                <div class="row align-items-center" data-anima="fade-bottom" data-time="1000">
                    <div class="col-lg-6 col-md-6">
                        <div class="title">
                            <h2>REAL ESTATE</h2>
                        </div>
                        <p>
                            Real estate investing has played a huge part in the success story of most billionaires
                            around the world today. It�s a proven and tested way for all kinds of investors to get ahead
                            and reach their financial and investment goals.


                        </p>

                    </div>
                    <div class="col-lg-6 col-md-6">
                        <img src="{{asset('media/box-2.jpg')}}" alt="" />
                    </div>
                </div>
            </div>
        </section>

        <img src="images/o2.jpg" alt="" width="100%" />

        <!-- TradingView Widget BEGIN -->
        <div class="tradingview-widget-container">
            <div class="tradingview-widget-container__widget"></div>
            <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/" rel="noopener nofollow"
                    target="_blank"><span class="blue-text">Track all markets on TradingView</span></a></div>
            <script type="text/javascript" src="{{asset('s3.tradingview.com/external-embedding/embed-widget-hotlists.js')}}"
                async>
                    {
                        "colorTheme": "light",
                            "dateRange": "12M",
                                "exchange": "US",
                                    "showChart": true,
                                        "locale": "en",
                                            "largeChartUrl": "",
                                                "isTransparent": false,
                                                    "showSymbolLogo": false,
                                                        "showFloatingTooltip": false,
                                                            "width": "400",
                                                                "height": "600",
                                                                    "plotLineColorGrowing": "rgba(41, 98, 255, 1)",
                                                                        "plotLineColorFalling": "rgba(41, 98, 255, 1)",
                                                                            "gridLineColor": "rgba(240, 243, 250, 0)",
                                                                                "scaleFontColor": "rgba(106, 109, 120, 1)",
                                                                                    "belowLineFillColorGrowing": "rgba(41, 98, 255, 0.12)",
                                                                                        "belowLineFillColorFalling": "rgba(41, 98, 255, 0.12)",
                                                                                            "belowLineFillColorGrowingBottom": "rgba(41, 98, 255, 0)",
                                                                                                "belowLineFillColorFallingBottom": "rgba(41, 98, 255, 0)",
                                                                                                    "symbolActiveColor": "rgba(41, 98, 255, 0.12)"
                    }
                </script>
        </div>
        <!-- TradingView Widget END -->
        <div class="container">
            <h1>INVESTMENT PLANS</h1>

        </div>

        <section class="section-base section-color">
            <div class="container">
                <div class="row" data-anima="fade-bottom" data-timeline="asc" data-time="2000">

                    <div class="columns">
                        <ul class="price">
                            <li class="header">BASIC PLAN</li>
                            <li class="grey" style="color: #00ccff"><b>0.3%</b> Daily For 6 Days</li>
                            <li style="background-color: #00ccff;"><b>Minimum Deposit </b> - $1,000</li>
                            <li style="background-color: #00ccff;"><b>Maximum Deposit </b> - $30,000</li>
                            <li style="background-color: #00ccff;"><b>Withdrawal </b> - After 6 Days</li>
                            <li style="background-color: #00ccff;"><b>Deposit Principle </b> - Included</li>
                            <li style="background-color: #00ccff;"><b>Support </b> - 24/7 Customer Support</li>
                            <li style="background-color: #00ccff;"><b>10%</b> - Referral Bonus</li>

                            <li class="grey"><a href="indexcca3.html?a=signup" class="button">Invest Now</a></li>
                        </ul>
                    </div>


                    <div class="columns">
                        <ul class="price">
                            <li class="header">SILVER PLAN</li>
                            <li class="grey" style="color: #00ccff"><b>0.7%</b> Daily For 15 Days</li>
                            <li style="background-color: #00ccff;"><b>Minimum Deposit </b> - $30,000</li>
                            <li style="background-color: #00ccff;"><b>Maximum Deposit </b> - $100,000</li>
                            <li style="background-color: #00ccff;"><b>Withdrawal </b> - After 15 Days</li>
                            <li style="background-color: #00ccff;"><b>Deposit Principle </b> - Included</li>
                            <li style="background-color: #00ccff;"><b>Support </b> - 24/7 Customer Support</li>
                            <li style="background-color: #00ccff;"><b>10%</b> - Referral Bonus</li>

                            <li class="grey"><a href="indexcca3.html?a=signup" class="button">Invest Now</a></li>
                        </ul>
                    </div>

                    <div class="columns">
                        <ul class="price">
                            <li class="header">DIAMOND PLAN</li>
                            <li class="grey" style="color: #00ccff"><b>1.0%</b> Daily For 4 Days</li>
                            <li style="background-color: #00ccff;"><b>Minimum Deposit </b> - $110,000</li>
                            <li style="background-color: #00ccff;"><b>Maximum Deposit </b> - $500,000</li>
                            <li style="background-color: #00ccff;"><b>Withdrawal </b> - After 4 Days</li>
                            <li style="background-color: #00ccff;"><b>Deposit Principle </b> - Included</li>
                            <li style="background-color: #00ccff;"><b>Support </b> - 24/7 Customer Support</li>
                            <li style="background-color: #00ccff;"><b>10%</b> - Referral Bonus</li>

                            <li class="grey"><a href="indexcca3.html?a=signup" class="button">Invest Now</a></li>
                        </ul>
                    </div>

                    <div class="columns">
                        <ul class="price">
                            <li class="header">GOLD PLAN</li>
                            <li class="grey" style="color: #00ccff"><b>15%</b> Daily For 8 Days</li>
                            <li style="background-color: #00ccff;"><b>Minimum Deposit </b> - $600,000</li>
                            <li style="background-color: #00ccff;"><b>Maximum Deposit </b> - $1,000,000</li>
                            <li style="background-color: #00ccff;"><b>Withdrawal </b> - After 8 Days</li>
                            <li style="background-color: #00ccff;"><b>Deposit Principle </b> - Included</li>
                            <li style="background-color: #00ccff;"><b>Support </b> - 24/7 Customer Support</li>
                            <li style="background-color: #00ccff;"><b>10%</b> - Referral Bonus</li>

                            <li class="grey"><a href="indexcca3.html?a=signup" class="button">Invest Now</a></li>
                        </ul>
                    </div>

                    <div class="columns">
                        <ul class="price">
                            <li class="header">PREMIUM PLAN</li>
                            <li class="grey" style="color: #00ccff"><b>2.14%</b> Daily For 16 Days</li>
                            <li style="background-color: #00ccff;"><b>Minimum Deposit </b> - $2,000,000</li>
                            <li style="background-color: #00ccff;"><b>Maximum Deposit </b> - $10,000,000</li>
                            <li style="background-color: #00ccff;"><b>Withdrawal </b> - After 16 Days</li>
                            <li style="background-color: #00ccff;"><b>Deposit Principle </b> - Included</li>
                            <li style="background-color: #00ccff;"><b>Support </b> - 24/7 Customer Support</li>
                            <li style="background-color: #00ccff;"><b>Referral Bonus </b> - 10%</li>

                            <li class="grey"><a href="indexcca3.html?a=signup" class="button">Deposit now</a></li>
                        </ul>
                    </div>



                    <div class="columns">
                        <ul class="price">
                            <li class="header">FIXED DEPOSIT</li>
                            <li class="grey" style="color: #00ccff"><b>1.43%</b> Daily For 1 Day</li>
                            <li style="background-color: #00ccff;"><b>Minimum Deposit </b> - $200,000</li>
                            <li style="background-color: #00ccff;"><b>Maximum Deposit </b> - $200,000</li>
                            <li style="background-color: #00ccff;"><b>Withdrawal </b> - After 1 Day</li>
                            <li style="background-color: #00ccff;"><b>Deposit Principle </b> - Included</li>
                            <li style="background-color: #00ccff;"><b>Support </b> - 24/7 Customer Support</li>
                            <li style="background-color: #00ccff;"><b>Referral Bonus </b> - 10%</li>

                            <li class="grey"><a href="indexcca3.html?a=signup" class="button">Deposit now</a></li>
                        </ul>
                    </div>




                    <div class="columns">
                        <ul class="price">
                            <li class="header">PARTNERSHIP STANDARD</li>
                            <li class="grey" style="color: #00ccff"><b>20%</b> Daily For 1 Day</li>
                            <li style="background-color: #00ccff;"><b>Minimum Deposit </b> - $100,000</li>
                            <li style="background-color: #00ccff;"><b>Maximum Deposit </b> - $500,000</li>
                            <li style="background-color: #00ccff;"><b>Withdrawal </b> - After 1 Day</li>
                            <li style="background-color: #00ccff;"><b>Deposit Principle </b> - Included</li>
                            <li style="background-color: #00ccff;"><b>Support </b> - 24/7 Customer Support</li>
                            <li style="background-color: #00ccff;"><b>Referral Bonus </b> - 10%</li>

                            <li class="grey"><a href="indexcca3.html?a=signup" class="button">Deposit now</a></li>
                        </ul>
                    </div>



                    <div class="columns">
                        <ul class="price">
                            <li class="header">GROUP PLAN</li>
                            <li class="grey" style="color: #00ccff"><b>1%</b> Daily For 3 Days</li>
                            <li style="background-color: #00ccff;"><b>Minimum Deposit </b> - $700,000</li>
                            <li style="background-color: #00ccff;"><b>Maximum Deposit </b> - $1,000,000</li>
                            <li style="background-color: #00ccff;"><b>Withdrawal </b> - After 3 Days</li>
                            <li style="background-color: #00ccff;"><b>Deposit Principle </b> - Included</li>
                            <li style="background-color: #00ccff;"><b>Support </b> - 24/7 Customer Support</li>
                            <li style="background-color: #00ccff;"><b>Referral Bonus </b> - 10%</li>

                            <li class="grey"><a href="indexcca3.html?a=signup" class="button">Deposit now</a></li>
                        </ul>
                    </div>






                    <!-- <div class="col-lg-4 anima">
                        <div class="cnt-box cnt-pricing-table">
                            <div class="top-area">
                                <h2> ACE PACKAGES</h2>
                                <div class="price"><span>2.5</span>%</div>
                                <p>DAILY FOR 6 DAYS</p>
                            </div>
                            <ul>
                                <li>Minimum deposit: $100</li>
                                <li>Maximum deposit: $4,499</li>
                                <li>Return on Investment: 15%</li>
                                <li>Deposit Principal: Included</li>
                                <li>Referral Bonus: 10%</li>
                            </ul>
                            <div class="bottom-area">
                                                                <a class="btn btn-border btn-xs" href="?a=signup">Deposit now</a>
                                                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 anima">
                        <div class="cnt-box cnt-pricing-table pricing-table-big">
                            <div class="top-area">
                                <h2>PREMIUM PACKAGES</h2>
                                <div class="price"><span>3.0</span>%</div>
                                <p>DAILY FOR 6 DAYS</p>
                            </div>
                            <ul>
                                      <li>Minimum deposit: $5,000</li>
                                <li>Maximum deposit: $9,999</li>
                                <li>Return on Investment: 18%</li>
                                <li>Deposit Principal: Included</li>
                                <li>Referral Bonus: 10%</li>
                            </ul>
                            <div class="bottom-area">
                                                                <a class="btn btn-border btn-xs" href="?a=signup">Deposit now</a>
                                                            </div>
                        </div>
                    </div>

                        <div class="col-lg-4 anima">
                        <div class="cnt-box cnt-pricing-table pricing-table-big">
                            <div class="top-area">
                                <h2>MASTERS PACKAGES</h2>
                                <div class="price"><span>3.5</span>%</div>
                                <p>DAILY FOR 6 DAYS</p>
                            </div>
                            <ul>
                                  <li>Minimum deposit: $10,000</li>
                                <li>Maximum deposit: $14,999</li>
                                <li>Return on Investment: 21%</li>
                                <li>Deposit Principal: Included</li>
                                <li>Referral Bonus: 10%</li>
                            </ul>
                            <div class="bottom-area">
                                                                <a class="btn btn-border btn-xs" href="?a=signup">Deposit now</a>
                                                            </div>
                        </div>
                    </div>


 <div class="col-lg-4 anima">
                        <div class="cnt-box cnt-pricing-table pricing-table-big">
                            <div class="top-area">
                                <h2>ELITES PACKAGES</h2>
                                <div class="price"><span>4.0</span>%</div>
                                <p>DAILY FOR 6 DAYS</p>
                            </div>
                            <ul>
                                  <li>Minimum deposit: $15,000</li>
                                <li>Maximum deposit: $Unlimited</li>
                                <li>Return on Investment: 24%</li>
                                <li>Deposit Principal: Included</li>
                                <li>Referral Bonus: 10%</li>
                            </ul>
                            <div class="bottom-area">
                                                                <a class="btn btn-border btn-xs" href="?a=signup">Deposit now</a>
                                                            </div>
                        </div>
                    </div>


                  <div class="col-lg-4 anima">
                        <div class="cnt-box cnt-pricing-table">
                            <div class="top-area">
                                <h2> PLAN 4</h2>
                                <div class="price"><span>100</span>%</div>
                                <p>AFTER 120 HOURS</p>
                            </div>
                            <ul>
                                <li>Minimum deposit: $10,000</li>
                                <li>Maximum deposit: Unlimited</li>
                                <li>Return on Investment: YES</li>
                                <li>Financial planning session</li>
                                <li>Deposit Principal: Included</li>
                                <li>Referral Bonus: 10%</li>
                            </ul>
                            <div class="bottom-area">
                                                                <a class="btn btn-border btn-xs" href="?a=signup">Deposit now</a>
                                                            </div>
                        </div>-->
                </div>
            </div>

            </div>
        </section>

        <img src="{{asset('images/o3.jpg')}}" alt="" width="100%" />
        <p style=" text-align: center; font-size: 24px; font-weight:bolder;">Click to watch </p>

        <center> <video width="90%" height="100%" src="{{asset('opu.mp4')}}" controls="">
            </video> </center>
        <section class="section-image light" data-parallax="scroll" data-image-src="{{asset('media/hd-2.jpg')}}">
            <div class="container">
                <div class="row" data-anima="fade-bottom" data-time="1000">
                    <div class="col-lg-6">
                        <div class="title">
                            <h2>Comprehensive services we provide</h2>
                            <p></p>
                        </div>
                        <p>
                            Robust, transparent benchmarks bridging the gap between traditional and digital asset
                            investment.
                        </p>
                        <a href="#" class="btn-text active">All services</a>
                    </div>
                    <div class="col-lg-6">
                        <div class="grid-list gap-40" data-columns="2" data-columns-xs="1">
                            <div class="grid-box">
                                <div class="grid-item">
                                    <div class="icon-box icon-box-left">
                                        <i class="im-pen"></i>
                                        <div class="caption">
                                            <h3>Electronic Execution</h3>
                                            <p>Our custom-built APIs provide clients access to more than 10 investment
                                                assets*</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid-item">
                                    <div class="icon-box icon-box-left">
                                        <i class="im-security-camera"></i>
                                        <div class="caption">
                                            <h3>Electronic Market Liquidity</h3>
                                            <p>We offer a variety of liquidity provisioning services in secondary
                                                markets</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid-item">
                                    <div class="icon-box icon-box-left">
                                        <i class="im-gears"></i>
                                        <div class="caption">
                                            <h3>Programmatic Execution</h3>
                                            <p>At {{ config('app.name') }} Finance investment, we have a dedicated Diversity & Inclusion
                                                working group led by Executive Chairman, KSENIA S�BERG. This
                                                cross-departmental group is tasked with proactively tracking and
                                                developing a range of initiatives to help track and boost gender
                                                balance, ethnic diversity and inclusivity within our organisation and
                                                the stock, forest and crypto trading industry.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid-item">
                                    <div class="icon-box icon-box-left">
                                        <i class="im-data-refresh"></i>
                                        <div class="caption">
                                            <h3>Learning</h3>
                                            <p>Provide issuers and investors our experience and insights</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid-item">
                                    <div class="icon-box icon-box-left">
                                        <i class="im-support"></i>
                                        <div class="caption">
                                            <h3>Investment Approach</h3>
                                            <p>Introduce investment opportunities to investors</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid-item">
                                    <div class="icon-box icon-box-left">
                                        <i class="im-coins"></i>
                                        <div class="caption">
                                            <h3>Bespoke Solutions</h3>
                                            <p>We aim to empower our clients by providing them with a ranges of
                                                innovative investment solutions. Our dedicated Sustainable Finance team
                                                works to offer a broad spectrum of investment solutions (packages) that
                                                range from the Arbitrage Basic Plan to the Premier Plan</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <img src="{{asset('images/o4.jpg')}}" alt="" width="100%" />
        <section class="section-base section-color">
            <div class="container">
                <div class="title" data-anima="fade-bottom" data-time="1000">
                    <h2>Our Supported Payment Gateways</h2>
                    <p></p>
                </div>
                <hr class="space-xs" />
                <div class="tab-box tab-top-right" data-tab-anima="fade-bottom" data-anima="fade-bottom"
                    data-time="1000">
                    <ul class="tab-nav">
                        <li class="active"><a href="#">Digital Assets</a></li>
                        <!-- <li><a href="#">Digital Assets</a></li>-->
                    </ul>
                    <div class="panel active">
                        <div class="grid-list" data-columns="4" data-columns-lg="2" data-columns-sm="1">
                            <div class="grid-box">
                                <div class="grid-item">
                                    <div class="cnt-box cnt-box-top-icon boxed">
                                        <img src="{{asset('images/btc.png')}}" alt="" style="width:150px;height:68px;">
                                        <div class="caption">
                                            <center>
                                                <h2>Bitcoin</h2>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid-item">
                                    <div class="cnt-box cnt-box-top-icon boxed">
                                        <img src="{{asset('images/eth.png')}}" alt="" style="width:150px;height:68px;">
                                        <style>
                                            img {
                                                display: block;
                                                margin-left: auto;
                                                margin-right: auto;
                                            }
                                        </style>
                                        <div class="caption">
                                            <center>
                                                <h2>Ethereum</h2>
                                            </center>
                                            <p>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid-item">
                                    <div class="cnt-box cnt-box-top-icon boxed">
                                        <img src="{{asset('images/usdt.png')}}" alt="" style="width:150px;height:68px;">
                                        <div class="caption">
                                            <center>
                                                <h2>USDT</h2>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid-item">
                                    <div class="cnt-box cnt-box-top-icon boxed">
                                        <img src="{{asset('images/bnb.png')}}" alt="" style="width:150px;height:68px;">
                                        <div class="caption">
                                            <center>
                                                <h2>BNB</h2>
                                            </center>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <br><br>
        <center>
        <script type="text/javascript">
        DukascopyApplet = {"type":"online_news","params":{"header":false,"borders":false,"defaultLanguage":"en","availableLanguages":["ar","bg","cs","de","en","es","fa","fr","he","hu","it","ja","ms","pl","pt","ro","ru","sk","sv","th","uk","zh"],"newsCategories":["finance","forex","stocks","commodities"],"width":"100%","height":"100%","adv":"popup"}};</script>
        <script type="text/javascript" src="https://freeserv-static.dukascopy.com/2.0/core.js"></script>
            <!-- <script type="text/javascript">
                DukascopyApplet = {
                    "type": "online_news",
                    "params": {
                        "header": false,
                        "borders": false,
                        "defaultLanguage": "en",
                        "availableLanguages": ["ar", "bg", "cs", "de", "en", "es", "fa", "fr", "he", "hu", "it", "ja", "ms", "pl", "pt", "ro", "ru", "sk", "sv", "th", "uk", "zh"],
                        "newsCategories": ["finance", "forex", "stocks", "company_news", "commodities"],
                        "width": "90%",
                        "height": "500",
                        "adv": "popup"
                    }
                };

            </script> -->
            <!-- <script type="text/javascript" src="{{asset('freeserv-static.dukascopy.com/2.0/core.js')}}"></script> -->
        </center>


        <section class="section-base">
            <div class="container">
                <div class="row align-items-center" data-anima="fade-bottom" data-time="1000">
                    <div class="col-lg-6 col-md-6">
                        <img src="{{asset('media/box-6.jpg')}}" alt="" />
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="title">
                            <h2>NEW FRONTIER OF FINANCE</h2>
                            <p></p>
                        </div>
                        <p>
                            {{ config('app.name') }} Finance is boldly charting a new path in a new frontier of finance, and helping
                            investors of all types achieve their financial goals. The CoinShares Group brings together a
                            wide range of financial products and services into a single brand that our clients can
                            depend on.
                        </p>
                        <a href="indexe47e.html?a=about" class="btn btn-xs">Learn more</a>
                    </div>
                </div>
                <hr class="space-sm" />
                <hr class="space visible-md" />
                <div class="row align-items-center" data-anima="fade-bottom" data-time="1000">
                    <div class="col-lg-6 col-md-6">
                        <div class="title">
                            <h2>Grow with the best</h2>
                            <p>OUR TEAM</p>
                        </div>
                        <p>
                            We are a team of finance professionals, portfolio managers, traders, and product innovators
                            who embrace digital disruption and believe in its potential to transform markets, economies,
                            and modern society.

                            Our leadership team has a proven track record in delivering innovative financial products
                            and services in emerging asset classes. We have managed billions of dollars for
                            institutional investors and built innovative financial products and funds for the world's
                            leading financial organizations. Oh, and we have a thing for bitcoin and it's technologies.
                        </p>

                    </div>
                    <div class="col-lg-6 col-md-6">
                        <img src="{{asset('media/box-7.png')}}" alt="" />
                    </div>
                </div>
            </div>
        </section>
        <section class="section-base section-color">
            <div class="container">
                <div class="row" data-anima="fade-bottom" data-time="1000">
                    <div class="col-lg-6">
                        <div class="title">
                            <h2>EARN UP TO 10% COMMISSION</h2>
                            <p>AFFILIATE MARKETING</p>
                        </div>
                    </div>
                    <div class="col-lg-6 align-right align-left-md">
                        <hr class="space-sm hidden-md" />
                        <a href="#" class="btn-text active">View services</a>
                    </div>
                </div>
                <hr class="space" />
                <div class="box-steps" data-anima="fade-bottom" data-time="1000">
                    <div class="step-item">
                        <span>1</span>
                        <div class="content">
                            <h3>Communication</h3>
                            <div>
                                <p>
                                    Communicate clearly and consistently. We build trusted and enduring relationships by
                                    being concise, credible, and direct.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="step-item">
                        <span>2</span>
                        <div class="content">
                            <h3>Accountability</h3>
                            <div>
                                <p>
                                    Take responsibility. We learn from our failures and successes, and follow through on
                                    our promises to our team and our clients.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="step-item">
                        <span>3</span>
                        <div class="content">
                            <h3>Integrity</h3>
                            <div>
                                <p>
                                    Do the right thing. We conduct ourselves and our work with honesty, determination
                                    and dedication to our mission.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

@stop

@section('scripts')
@endsection
