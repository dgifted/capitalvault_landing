@extends('layouts.master')

@section('title', 'Crypto assets plans')
@section('styles')
@stop

@section('header')
<header class="header-image ken-burn-center light" data-parallax="true" data-natural-height="500" data-natural-width="1920" data-bleed="0" data-image-src="media/hd-wide-5.jpg" data-offset="0">
        <div class="container">
            <h1>CRYPTO ASSET PRICING PLANS</h1>
            <h2>The simplest way to invest in crypto</h2>
        </div>
    </header>
@stop
@section('content')
<section class="section-base section-color">
            <div class="container">
                <div class="row" data-anima="fade-bottom" data-timeline="asc" data-time="2000">
                    <div class="col-lg-4 anima">
                        <div class="cnt-box cnt-pricing-table">
                            <div class="top-area">
                                <h2>INAUGURATE PLAN</h2>
                                <div class="price"><span>7</span>%</div>
                                <p>DAILY FOR 30 DAYS</p>
                            </div>
                            <ul>
                                <li>Minimum deposit: $30.00</li>
                                <li>Maximum deposit: $50,000.00</li>
                                <li>Return on Investment: 210%</li>
                                <li>No tax deductions</li>
                                <li>Financial planning session</li>
                                <li>Deposit Principal: Included</li>
                            </ul>
                            <div class="bottom-area">
                                <a class="btn btn-border btn-xs" href="indexcca3.html?a=signup">Deposit now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 anima">
                        <div class="cnt-box cnt-pricing-table pricing-table-big">
                            <div class="top-area">
                                <h2>VITAL PLAN</h2>
                                <div class="price"><span>8</span>%</div>
                                <p>DAILY FOR 28 DAYS</p>
                            </div>
                            <ul>
                                <li>Minimum deposit: $1,300</li>
                                <li>Maximum deposit: $120,000.00</li>
                                <li>Return on Investment: 224%</li>
                                <li>No tax deductions</li>
                                <li>Financial planning session</li>
                                <li>Deposit Principal: Included</li>
                            </ul>
                            <div class="bottom-area">
                                <a class="btn btn-border btn-xs" href="indexcca3.html?a=signup">Deposit now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 anima">
                        <div class="cnt-box cnt-pricing-table">
                            <div class="top-area">
                                <h2>EXTREME PLAN</h2>
                                <div class="price"><span>9.6</span>%</div>
                                <p>DAILY FOR 30 DAYS</p>
                            </div>
                            <ul>
                                <li>Minimum deposit: $10,000.00</li>
                                <li>Maximum deposit: $500,000.00</li>
                                <li>Return on Investment: 288%</li>
                                <li>Financial planning session</li>
                                <li>Deposit Principal: Included</li>
                            </ul>
                            <div class="bottom-area">
                                <a class="btn btn-border btn-xs" href="indexcca3.html?a=signup">Deposit now</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
@stop

@section('scripts')
@endsection